#Responsive Site Plan - S-Town Basic
1. Create a page that contains some general information about the podcast
2. Include a couple different sections on the page for information
   * Logo, Navigation
   * Main callout
   * About the Show
   * Media
   * Credits/Contact
3. Write styles to make it look nicer
4. Media Queries for different devices/screen sizes
   * Max 1025px (iPad Landscape)
   * Max 769px (iPad Portrait)
   * Max 700px (Phones)




##Content

###Main Callout
S-Town Logo

John despises his Alabama town and he decides to do something about it. He asks a reporter to investigate the son of a wealthy family who’s allegedly been bragging that he got away with murder. But then someone else ends up dead, sparking a nasty feud, a hunt for hidden treasure, and an unearthing of the mysteries of one man’s life.


###About the Show
S-Town is a new podcast from Serial and This American Life, hosted by Brian Reed, about a man named John who despises his Alabama town and decides to do something about it. He asks Brian to investigate the son of a wealthy family who’s allegedly been bragging that he got away with murder. But then someone else ends up dead, and the search for the truth leads to a nasty feud, a hunt for hidden treasure, and an unearthing of the mysteries of one man’s life.

Brian, a longtime This American Life producer, started reporting this story more than three years ago, when he got an email from John with the subject line “John B McLemore lives in Shittown Alabama.”

S-Town is part of Serial Productions, a production company from Serial and This American Life.


###Media (Imagery)
John B
Maze Shot
Tyler Goodson

###Credits/Contact
Logos and a contact email



##Asset Information:
Background Image: provided

Fallback Background Colour: #efe8d8

Text Colour: #000000

Font Family: Google Font - Goudy Bookletter 1911

Navigation Link Background: #a2b8a3

Navigation Link Hover: #b2c7b3

Navigation Link Colour: #000000

Regular Link Colour: #c09a53

<meta name="viewport" content="width=device-width, height=device-height">



##Lets add this to the code

<!--
          _
        _/_\_
      _/_/ \_\_
     /_/     \_\
    /_/       \_\  Fixing an old clock can be maddening.
      |  .-.  |    You’re constantly wondering if you’ve just spent hours
      | / / \ |    going down a path that will take you nowhere.
      | \ \ / |    At every moment, you have to decide
      |  '-'  |    if you’re wasting your time.
      '- - - -'    Or not.
         | |
         | |
         | |
         /\|
         \/|
          /\
          \/
-->
